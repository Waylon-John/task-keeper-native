<?php
    session_start();
    require_once("../database/db.php");
    require_once("autoload.php");
    $user = new User($database);

    if(!empty($_POST)){
        $user->userRegister($_POST['username'],$_POST['profilename'],$_POST['password']);
        // var_dump($_POST);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Task Keeper</title>
</head>
<body>
    <div class="container center section">
        <h4>Enter details for Account Registration</h4>
        <form action="register.php" method="post">
            <input type="text" placeholder="Enter your username" name="username" required/>
            <input type="text" placeholder="Enter your Display name" name="profilename" required/>
            <input type="password" placeholder="Enter your password" name="password" required/>
            <button class="waves-effect waves-light btn-small" type="submit">Submit Details</button>
        </form>
        <h5><a href="../index.php">Go back to Login Page</a></h5>
    </div>
</body>
</html>