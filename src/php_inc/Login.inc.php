<?php

class Login {
    private $username;
    private $password;
    
    function __construct($username,$password){
        $this->username = $username;
        $this->password = $password;
    }

    function accountLogin($db) {
        $stmt = $db->prepare("select user_id,password from users where username = ?");
        $stmt->bind_param('s',$this->username);
        $stmt->execute();
        $stmt->bind_result($id,$password);
        while($stmt->fetch()){
            if($this->password == $password){
                $_SESSION['user'] = $id;
            } else {
                header("location: src/extrapages/loginError.php");
            }
        } $stmt->close();
    }
}

?>