<?php
    class User {
        private $db;
        function __construct($database) {
            $this->db = $database;
        }

        function userDisplay($id) {
            $stmt = $this->db->prepare('select username,profilename from users where user_id = ?');
            $stmt->bind_param("i",$id);
            $stmt->execute();
            $stmt->bind_result($username,$profilename);
            $profile = [];
            while($stmt->fetch()){
                array_push($profile,['username'=>$username,'displayName'=>$profilename]);
            } return $profile;

            $stmt->close();
        }

        function userRegister($username,$profilename,$password){
            $stmt = $this->db->prepare('select username from users');
            $stmt->execute();
            $stmt->bind_result($existingusername);
            $stmt->store_result();
            while($stmt->fetch()){
                if($username == $existingusername){
                    header("location: extrapages/registerFail.php");    
                    die();
                }  
            } $stmt->close();
            $stmt2 = $this->db->prepare('insert into users(username,profilename,password) values (?,?,?)');
            $stmt2->bind_param("sss",$username,$profilename,$password);
            $stmt2->execute();
            $stmt2->close();
            header('location: extrapages/registerSuccess.php');
        }
    }
?>