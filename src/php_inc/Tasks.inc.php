<?php
    class Tasks{
        private $db;
        function __construct($db){
            $this->database = $db;
        }

        function addTask($task,$uid){
            $stmt = $this->database->prepare("insert into tasks(user_id,taskdesc) values(?,?)");
            $stmt->bind_param('is',$uid,$task);
            $stmt->execute();
            $stmt->close();
        }
        
        function displayTask($id){
            $stmt = $this->database->prepare("select taskdesc,time_added,task_id from tasks where user_id = ?");
            $stmt->bind_param('i',$id);
            $stmt->execute();
            $stmt->bind_result($task,$time_added,$taskid);
            $display = [];
            while($stmt->fetch()){
                array_push($display,['tasks'=>$task,'time'=>$time_added,'taskID'=>$taskid]);
            } return $display;
            $stmt->close();
        }

        function unpinTask($taskID,$uid){
            $stmt = $this->database->prepare('delete from tasks where task_id = ?');
            $stmt->bind_param('i',$taskID);
            $stmt->execute();
            $stmt->close();
            header('location: ../src/profile.php?id='.$uid);
        }
    }
?>