<?php
    session_start();
    require_once("../database/db.php");
    require_once("autoload.php");
    $user = new User($database);
    $userTask = new Tasks($database);
    $id = $_GET['id'];

    if(isset($_POST['logoutButton'])){
        unset($_SESSION['user']);
    }

    if(isset($_GET['taskid'])){
        $userTask->unpinTask($_GET['taskid'],$id);
    }

    if(isset($_POST['pinTask'])){
        $userTask->addTask($_POST['task'],$_POST['id']);
    }   

    if(!isset($_SESSION['user'])){
        header("location: ../index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Task Keeper</title>
</head>
<body>
    <div class="container section">
        <?php foreach($user->userDisplay($id) as $userprofile): ?> 
            <form action='profile.php' method="post">
                <h3>
                    Hello <?=$userprofile['displayName'] ?>
                    <input type="submit" value="Logout" name="logoutButton" class="waves-effect waves-light btn-small red right"/>
                </h3>
            </form>
        <?php endforeach ?>
            <form class="col s15" action="profile.php?id=<?= $_SESSION['user']?>" method="post">
                <div class="row">
                    <div class="input-field col s10">
                        <i class="material-icons prefix">note_add</i>
                        <input id="icon_prefix" type="text" class="validate" name="task" placeholder="Pin a Task" required>
                        <input type="hidden" name="id" value="<?=$id?>" />
                    </div>
                    <div class="input-field col s2">
                        <input type="submit" value="Add" name="pinTask" class="waves-effect waves-light btn-small"/>
                    </div>
                </div>
            </form>
    </div>
    <div class="container">
        <div class= "center"><h4 class="blue-text">My Pinned Tasks</h4></div>
        <ol class="collection">
            <?php foreach($userTask->displayTask($id) as $taskList): ?>
            <li class="collection-item">
                <h5 class="title"><?=$taskList['tasks']?></h5>
                <span>
                    Date/Time added: <?=$taskList['time']?> |  
                    <a href="profile.php?id=<?=$id?>&taskid=<?=$taskList['taskID']?>">Unpin Task</a>
                </span>
            </li>
            <?php endforeach; ?>
        </ol>
        
    </div>
</body>
</html>