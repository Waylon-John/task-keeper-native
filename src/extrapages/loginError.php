<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Test Blog</title>
</head>
<body>
    <div class="divider"></div>
    <div class="container center section">
        <h4>Incorrect Credentials!! Please try again.</h4>
        <h5><a href="../../index.php">Return to Login Page</a></h5>
    </div>
</body>
</html>