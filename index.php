<?php
    require_once("src/autoload.php");
    require_once("database/db.php");
    session_start();
    if(!empty($_POST)){
        $loginCredentials = new Login($_POST['username'],$_POST['password']);
        $loginCredentials->accountLogin($database);
    }

    if(isset($_SESSION['user'])){
        header("location: src/profile.php?id=".$_SESSION['user']);
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>TasKeeper</title>
</head>
<body>
    <div class='container center section'>
        <h3>Welcome to TasKeeper</h3>
        <form action="index.php" method="post">
            <input type="text" name="username" placeholder = "username" required/>
            <input type="password" name="password" placeholder = "password" required/>
            <button class="waves-effect waves-light btn-small" type="submit">Login</button>
        </form>
    </div>
    <div class="divider"></div>
    <div class="container center section">
        <h4><a href="src/register.php">Click here to register an account!!</a></h4>
    </div>
</body>
</html>